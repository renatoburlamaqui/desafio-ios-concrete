//
//  Extentions.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit

extension BaseViewController {
    
    func showMessageAlert(message:String, title:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) { (alert) in}
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}