//
//  NavigationUtil.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 01/07/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit

class NavigationUtil: NSObject {
    
    static let shared = NavigationUtil()
    
    //MARK - Main Storyboard
    func getHomeViewController() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("homeVC") as! HomeViewController
        return vc
    }
    
    func getPullRequestVC() -> PullRequestsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("pullRequestVC") as! PullRequestsViewController
        return vc
    }
}

