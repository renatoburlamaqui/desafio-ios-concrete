//
//  ViewController.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import ObjectMapper
import Haneke

class HomeViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var homeTableView: UITableView!
    
    var repositoriesList: RepositoryList = RepositoryList()
    private var currentPage = 1
    private var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.loadInfo()
    }
    
    func loadInfo() {
        self.showActivityIndicator()
        self.isLoading = true
        GithubApi.shared.getMostPopularJavaRepositories(currentPage) { (response) in
            self.isLoading = false
            self.hideActivityIndicator()
            
            switch(response.result){
            case .Success(let JSON):
                self.repositoriesList = Mapper<RepositoryList>().map(JSON)!
                self.homeTableView.reloadData()
                
                break
            case .Failure(_):
                self.handleGenericError(response)
                break
                
            }
        }
    }
    
    func loadNextPage() {
        self.currentPage += 1
        self.addRepositoriesWithPage(self.currentPage)
    }
    
    func addRepositoriesWithPage(page:Int) {
        self.isLoading = true
        GithubApi.shared.getMostPopularJavaRepositories(page) { (response) in
            self.isLoading = false
            self.hideActivityIndicator()
            switch(response.result){
            case .Success(let JSON):
                let newRepositories = Mapper<RepositoryList>().map(JSON)!
                self.repositoriesList.addRepositories(newRepositories)
                self.homeTableView.reloadData()
                break
            case .Failure(_):
                self.handleGenericError(response)
                break
                
            }
        }
    }

    //MARK - TableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let repositoryCell = tableView.dequeueReusableCellWithIdentifier("repositoryCell", forIndexPath: indexPath) as! RepositoryTableViewCell
        
        let repositorySelected = self.repositoriesList.repositories[indexPath.row]
        
        repositoryCell.desc = repositorySelected.desc
        repositoryCell.name = repositorySelected.name
        repositoryCell.numberStars = repositorySelected.numberStars.stringValue
        repositoryCell.numberBranchs = repositorySelected.numberForks.stringValue
        
        if let owner = repositorySelected.owner {
            repositoryCell.username = owner.username
            repositoryCell.userImageView.hnk_setImageFromURL(NSURL(string:owner.imageUrl)!)
        }
        
        repositoryCell.userFullname = repositorySelected.fullname
        
        return repositoryCell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoriesList.repositories.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    //MARK - ScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let actualPosition = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height - 1500
        if (actualPosition >= contentHeight) {
            if self.isLoading == false {
                self.loadNextPage()
            }
        }
    }
    
    //MARK - TableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let repositorySelected = self.repositoriesList.repositories[indexPath.row]
        self.performSegueWithIdentifier("goToPullRequestViewController", sender: repositorySelected)
    }
    
    //MARK - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToPullRequestViewController" {
            let repositorySelected = sender as! Repository
            
            let destinationVC = segue.destinationViewController as! PullRequestsViewController
            destinationVC.ownerName = repositorySelected.owner!.username
            destinationVC.repository = repositorySelected.name
            
        }
    }
}

