//
//  PullRequest.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 30/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: BaseModel {
    var title: String = ""
    var fullname: String = ""
    var body: String = ""
    var user: Owner?
    var createDate: NSDate?
    var externalUrl: String = ""
    
    
    required init?(_ map: Map) {
        super.init()
    }
    
    override init() {
        super.init()
    }
    
    func getFormattedCreatedDate() -> String {
        
        if let createDate = self.createDate {
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
            dateFormatter.dateStyle = .MediumStyle
            //        dateFormatter.dateFormat = "dd/MM/yyyy"
            return dateFormatter.stringFromDate(createDate)
        }
        return ""
    }
    
    // Mappable
    override func mapping(map: Map) {
        title <- map["title"]
        fullname <- map["full_name"]
        body <- map["body"]
        user <- map["user"]
        createDate <- (map["created_at"], DateTransform())
        externalUrl <- map["html_url"]
    }
}
