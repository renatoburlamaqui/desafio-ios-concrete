//
//  Repository.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 30/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: BaseModel {
    var name: String = ""
    var numberForks: NSNumber = 0
    var numberStars: NSNumber = 0
    var fullname: String = ""
    var desc: String = ""
    var owner: Owner?
    
    
    required init?(_ map: Map) {
        super.init()
    }
    
    override init() {
        super.init()
    }
    
    
    // Mappable
    override func mapping(map: Map) {
        name <- map["name"]
        numberForks  <- map["forks_count"]
        fullname <- map["full_name"]
        desc <- map["description"]
        numberStars <- map["stargazers_count"]
        owner <- map["owner"]
    }
}
