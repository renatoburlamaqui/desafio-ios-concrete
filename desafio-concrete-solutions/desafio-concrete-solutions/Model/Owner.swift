//
//  Owner.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 30/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import ObjectMapper

class Owner: BaseModel {
    var imageUrl: String = ""
    var username: String = ""
    var fullname: String = ""

    
    required init?(_ map: Map) {
        super.init()
    }
    
    override init() {
        super.init()
    }
    
    
    // Mappable
    override func mapping(map: Map) {
        imageUrl <- map["avatar_url"]
        username  <- map["login"]
        fullname <- map["type"]
    }
}
