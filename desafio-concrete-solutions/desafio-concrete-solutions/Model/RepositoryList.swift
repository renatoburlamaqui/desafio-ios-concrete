//
//  RepositoryList.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 30/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import ObjectMapper

class RepositoryList: BaseModel {
    
    var repositories: [Repository] = [Repository]()
    
    required init?(_ map: Map) {
        super.init()
    }
    
    override init() {
        super.init()
    }
    
    
    func addRepositories(list:RepositoryList) {
        self.repositories.appendContentsOf(list.repositories)
    }
    
    // Mappable
    override func mapping(map: Map) {
        repositories <- map["items"]
    }
}
