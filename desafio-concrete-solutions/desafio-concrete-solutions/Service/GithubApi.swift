//
//  GithubApi.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import Alamofire

class GithubApi: BaseApi {

    static let shared = GithubApi()
    
    var baseUrl = "https://api.github.com"
    
    func getMostPopularJavaRepositories(page:Int, completionHandler: Response<AnyObject, NSError> -> Void) {
        let path = "/search/repositories?q=language:Java&sort=stars&page=\(page)"
        self.getFromServer(baseUrl + path, completionHandler: completionHandler, headers: ["":""])
    }
    
    func getPullRequestsWithRepository(repository:String,owner:String, completionHandler: Response<AnyObject, NSError> -> Void) {
        let path = "/repos/\(owner)/\(repository)/pulls"
        self.getFromServer(baseUrl + path, completionHandler: completionHandler, headers: ["":""])
    }
    
}
