//
//  BaseService.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

struct Configs {
    static let isDebug = false
}


class BaseApi: NSObject {
    
    func getFromServer(url: String, completionHandler: Response<AnyObject, NSError> -> Void, headers: [String: String]) {
        Alamofire.request(.GET, url, headers: headers, encoding: .JSON)
            .responseJSON { response in
                
                if Configs.isDebug == true {
                    debugPrint(response.request)  // original URL request
                    debugPrint(response.response) // URL response
                    debugPrint(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
                }
                
                completionHandler(response)
        }
    }
    
    func getFromServer(url: String, params: [String: AnyObject], completionHandler: Response<AnyObject, NSError> -> Void, headers: [String: String]) {
        Alamofire.request(.GET, url).validate(statusCode: 200..<300)
            .responseJSON { response in
                if Configs.isDebug == true {
                    debugPrint(response.request)  // original URL request
                    debugPrint(response.response) // URL response
                    debugPrint(response.data)     // server data
                    debugPrint(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        debugPrint("JSON: \(JSON)")
                    }
                }

                completionHandler(response)
        }
    }
    
    func postToServer(params: [String: AnyObject], url: String, completionHandler: Response<AnyObject, NSError> -> Void, headers: [String: String]) {
        
        Alamofire.request(.POST, url, parameters: params, encoding: .JSON, headers: headers).validate(statusCode: 200..<300)
            .responseJSON { response in
                completionHandler(response)
                
                if Configs.isDebug == true {
                    debugPrint(response.request)  // original URL request
                    debugPrint(response.response) // URL response
                    debugPrint(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        debugPrint("JSON: \(JSON)")
                    }
                }
        }
    }
    
    func putToServer(params: [String: AnyObject], url: String, completionHandler: Response<AnyObject, NSError> -> Void, headers: [String: String]) {
        Alamofire.request(.PUT, url, parameters: params, encoding: .JSON, headers: headers).validate(statusCode: 200..<300)
            .responseJSON { response in
                
                if Configs.isDebug == true {
                    debugPrint(response.request)  // original URL request
                    debugPrint(response.response) // URL response
                    debugPrint(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        debugPrint("JSON: \(JSON)")
                    }
                }
                
                completionHandler(response)
        }
    }
    
    func deleteToServer(url: String, completionHandler: Response<AnyObject, NSError> -> Void, headers: [String: String]) {
        Alamofire.request(.DELETE, url, encoding: .JSON, headers: headers).validate(statusCode: 200..<300)
            .responseJSON { response in
                
                if Configs.isDebug == true {
                    debugPrint(response.request)  // original URL request
                    debugPrint(response.response) // URL response
                    debugPrint(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        debugPrint("JSON: \(JSON)")
                    }
                }
                
                completionHandler(response)
        }
    }
}
