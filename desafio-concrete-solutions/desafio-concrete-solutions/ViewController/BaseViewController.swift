//
//  BaseViewController.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //remove text back button
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showActivityIndicator(){
        self.hideKeyboard()
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    func handleGenericError(response: Response<AnyObject, NSError>!){
        let title = "Erro"
        var message = "Houve um problema no servidor, tente novamente mais tarde."
        
        if response.response?.statusCode == 404 {
            message = "Servidor não encontrado."
        }
        
        if response.response?.statusCode == 403 {
            message = "Você precisa estar logado para acessar esta área."
        }
        self.showMessageAlert(message, title: title)
    }
    
    func hideActivityIndicator(){
        PKHUD.sharedHUD.hide(animated: true)
    }
    
    func hideKeyboard(){
        let subviews = self.view.subviews
        if subviews.count > 0{
            for view in subviews{
                if view is UITextView || view is UITextField {
                    view.resignFirstResponder()
                }else if (view.subviews.count > 0){
                    self.resignSubviews(view)
                }
            }
        }
    }
    
    private func resignSubviews(view: UIView){
        for view in view.subviews{
            if view is UITextView || view is UITextField {
                view.resignFirstResponder()
            }else if (view.subviews.count > 0){
                self.resignSubviews(view)
            }
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
