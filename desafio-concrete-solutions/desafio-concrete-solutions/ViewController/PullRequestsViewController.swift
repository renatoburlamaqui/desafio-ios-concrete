//
//  PullRequestsViewController.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 30/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequestsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var pullRequestsTableView: UITableView!
    
    var pullRequestsList: Array<PullRequest> = Array<PullRequest>()
    
    var repository = ""
    var ownerName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = repository
        
        self.loadInfo()
        // Do any additional setup after loading the view.
    }
    
    func loadInfo() {
        self.showActivityIndicator()
        
        GithubApi.shared.getPullRequestsWithRepository(repository, owner: ownerName) { (response) in
            self.hideActivityIndicator()
            switch(response.result){
            case .Success(let JSON):
                print(JSON)
                
                self.pullRequestsList =  Mapper<PullRequest>().mapArray(JSON)!
                
                self.pullRequestsTableView.reloadData()
                
                break
            case .Failure(_):
                self.handleGenericError(response)
                break
                
            }
        }
    }
    
    //MARK - TableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let pullRequestCell = tableView.dequeueReusableCellWithIdentifier("pullRequestCell", forIndexPath: indexPath) as! PullRequestTableViewCell
        
        let pullRequestSelected = self.pullRequestsList[indexPath.row]
        
        pullRequestCell.body = pullRequestSelected.body
        pullRequestCell.name = pullRequestSelected.title
        
        if let user = pullRequestSelected.user {
            pullRequestCell.username = user.username
            pullRequestCell.userImageView.hnk_setImageFromURL(NSURL(string:user.imageUrl)!)
        }
        
        pullRequestCell.userFullname = pullRequestSelected.getFormattedCreatedDate()
        
        return pullRequestCell
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequestsList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 110
    }
    
    //MARK - TableViewDataSource
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequestSelected = self.pullRequestsList[indexPath.row]
        self.callSafariWithUrl(pullRequestSelected.externalUrl)
    }
    
    func callSafariWithUrl(urlString:String) {
        if let url = NSURL(string:urlString){
            UIApplication.sharedApplication().openURL(url)
        }else{
            showMessageAlert("Houve um erro ao tentar abrir a url desejada", title: "Erro")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
