//
//  RepositoryTableViewCell.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var numberBranchsLabel: UILabel!
    @IBOutlet weak var numberStarsLabel: UILabel!
    
    //User Outlets
    @IBOutlet weak var userImageView: RoundedImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    
    var name = "" {
        didSet {
            self.nameLabel.text = self.name
        }
    }
    
    var desc = "" {
        didSet {
            self.descriptionLabel.text = self.desc
        }
    }
    
    var numberBranchs = "" {
        didSet {
            self.numberBranchsLabel.text = self.numberBranchs
        }
    }
    
    var numberStars = "" {
        didSet {
            self.numberStarsLabel.text = self.numberStars
        }
    }
    
    var userImage = UIImage() {
        didSet {
            self.userImageView.image = self.userImage
        }
    }
    
    var username = "" {
        didSet {
            self.usernameLabel.text = self.username
        }
    }
    
    var userFullname = "" {
        didSet {
            self.fullnameLabel.text = userFullname
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
