//
//  PullRequestTableViewCell.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 30/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    //User Outlets
    @IBOutlet weak var userImageView: RoundedImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    
    var name = "" {
        didSet {
            self.nameLabel.text = self.name
        }
    }
    
    var body = "" {
        didSet {
            self.bodyLabel.text = self.body
        }
    }
    
    var userImage = UIImage() {
        didSet {
            self.userImageView.image = self.userImage
        }
    }
    
    var username = "" {
        didSet {
            self.usernameLabel.text = self.username
        }
    }
    
    var userFullname = "" {
        didSet {
            self.fullnameLabel.text = userFullname
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
