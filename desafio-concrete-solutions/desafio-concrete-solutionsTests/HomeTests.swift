//
//  HomeTests.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 01/07/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import XCTest
@testable import desafio_concrete_solutions

class HomeTests: XCTestCase {
    
    var vc : HomeViewController!
    let TIMEOUT :NSTimeInterval = 15
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.vc =  NavigationUtil.shared.getHomeViewController()
        let _ = vc.view
        vc.viewDidLoad()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHomeOutlets() {
        XCTAssertNotNil(vc.homeTableView)
    }
    
    func testRepositoryCellOutlets() {
        let qtd = 5
        vc.repositoriesList = self.createRepositoriesMockWithCapacity(qtd)
        vc.homeTableView.reloadData()
        
        if let cell = vc.homeTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as? RepositoryTableViewCell {
            XCTAssertNotNil(cell.nameLabel)
            XCTAssertNotNil(cell.descriptionLabel)
            XCTAssertNotNil(cell.numberBranchsLabel)
            XCTAssertNotNil(cell.numberStarsLabel)
            XCTAssertNotNil(cell.userImageView)
            XCTAssertNotNil(cell.usernameLabel)
            XCTAssertNotNil(cell.fullnameLabel)
        }
    }
    
    func testIfProprietsHomeShowCorrectInformations() {
        let qtd = 5
        
        vc.repositoriesList = self.createRepositoriesMockWithCapacity(qtd)
        vc.homeTableView.reloadData()
        
        if let cell = vc.homeTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as? RepositoryTableViewCell {
            cell.name = "Teste"
            cell.desc = "Desc Teste"
            cell.username = "Username Teste"
            cell.userFullname = "Teste Full name"
            cell.numberStars = "10"
            cell.numberBranchs = "13"
            
            XCTAssertEqual(cell.name, cell.nameLabel.text)
            XCTAssertEqual(cell.desc, cell.descriptionLabel.text)
            XCTAssertEqual(cell.username, cell.usernameLabel.text)
            XCTAssertEqual(cell.userFullname, cell.fullnameLabel.text)
            XCTAssertEqual(cell.numberStars, cell.numberStarsLabel.text)
            XCTAssertEqual(cell.numberBranchs, cell.numberBranchsLabel.text)
        }
    }
    
    func testHomeTableViewRenderRepositoryCell() {
        let qtd = 5
        vc.repositoriesList = self.createRepositoriesMockWithCapacity(qtd)
        vc.homeTableView.reloadData()
        
        let cell = vc.homeTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0))
        XCTAssertTrue(cell is RepositoryTableViewCell)
    }
    
    func testAddRepositoriesInHomeSourceList() {
        let qtd = 5
        
        vc.repositoriesList = self.createRepositoriesMockWithCapacity(qtd)
        vc.repositoriesList.addRepositories(vc.repositoriesList)
        
        XCTAssertEqual(vc.repositoriesList.repositories.count, qtd * 2)
        
    }
    
    func testHomeNumberItensTableViewIsTheSameInArray() {
        let qtd = 5
        
        vc.repositoriesList = self.createRepositoriesMockWithCapacity(qtd)
        vc.homeTableView.reloadData()
        XCTAssertEqual(vc.homeTableView.numberOfRowsInSection(0),vc.repositoriesList.repositories.count, "Número de rows na tabela deve ser igual a \(String(qtd))")
    }
    
    private func createRepositoriesMockWithCapacity(qtd:Int) -> RepositoryList {
        let listRepositoriesMock = RepositoryList()
        
        for _ in 1...qtd {
            let newRepository = Repository()
            newRepository.desc = "testeeeee descricao"
            newRepository.fullname = "teste fullname"
            listRepositoriesMock.repositories.append(newRepository)
        }
        return listRepositoriesMock
    }
    
    //Performance Tests
    
    func testDateFormatterPerformance() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .LongStyle
        dateFormatter.timeStyle = .ShortStyle
        
        let date = NSDate()
        
        measureBlock() {
            let _ = dateFormatter.stringFromDate(date)
        }
    }
    
}
