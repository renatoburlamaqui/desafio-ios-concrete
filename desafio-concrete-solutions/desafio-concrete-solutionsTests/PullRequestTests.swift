//
//  PullRequestTests.swift
//  desafio-concrete-solutions
//
//  Created by Patrícia Pampanelli on 01/07/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import XCTest
@testable import desafio_concrete_solutions

class PullRequestTests: XCTestCase {
    
    var vc : PullRequestsViewController!
    let TIMEOUT :NSTimeInterval = 15
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.vc =  NavigationUtil.shared.getPullRequestVC()
        let _ = vc.view
        vc.viewDidLoad()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPullRequestsViewControllerOutlets() {
        XCTAssertNotNil(vc.pullRequestsTableView)
    }
    
    func testRepositoryCellOutlets() {
        let qtd = 5
        
        vc.pullRequestsList = self.createPullRequestsMockWithCapacity(qtd)
        vc.pullRequestsTableView.reloadData()
        
        if let cell = vc.pullRequestsTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as? PullRequestTableViewCell {
            XCTAssertNotNil(cell.nameLabel)
            XCTAssertNotNil(cell.bodyLabel)
            XCTAssertNotNil(cell.fullnameLabel)
            XCTAssertNotNil(cell.userImageView)
            XCTAssertNotNil(cell.usernameLabel)
            XCTAssertNotNil(cell.userImageView)
        }
    }
    
    func testIfProprietsPullRequestShowCorrectInformations() {
        let qtd = 5
        
        vc.pullRequestsList = self.createPullRequestsMockWithCapacity(qtd)
        vc.pullRequestsTableView.reloadData()
        
        if let cell = vc.pullRequestsTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as? PullRequestTableViewCell {
            cell.name = "Teste"
            cell.body = "body Teste"
            cell.username = "Username Teste"
            cell.userFullname = "Teste Full name"

            
            XCTAssertEqual(cell.name, cell.nameLabel.text)
            XCTAssertEqual(cell.body, cell.bodyLabel.text)
            XCTAssertEqual(cell.username, cell.usernameLabel.text)
            XCTAssertEqual(cell.userFullname, cell.fullnameLabel.text)
        }
    }
    
    func testPullRequestTableViewRenderPullRequestCell() {
        let qtd = 5
        vc.pullRequestsList = self.createPullRequestsMockWithCapacity(qtd)
        vc.pullRequestsTableView.reloadData()
        
        let cell = vc.pullRequestsTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0))
        XCTAssertTrue(cell is PullRequestTableViewCell)
    }
    
    private func createPullRequestsMockWithCapacity(qtd:Int) -> [PullRequest] {
        var listPullRequestMock = [PullRequest]()
        
        for _ in 1...qtd {
            let newPullRequest = PullRequest()
            newPullRequest.title = "testeeeee Title"
            newPullRequest.body = "testeeeee body"
            newPullRequest.fullname = "teste fullname"
            listPullRequestMock.append(newPullRequest)
        }
        return listPullRequestMock
    }
    
}
