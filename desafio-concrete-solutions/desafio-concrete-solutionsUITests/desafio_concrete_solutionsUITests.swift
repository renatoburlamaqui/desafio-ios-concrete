//
//  desafio_concrete_solutionsUITests.swift
//  desafio-concrete-solutionsUITests
//
//  Created by Patrícia Pampanelli on 29/06/16.
//  Copyright © 2016 Renato Medina. All rights reserved.
//

import XCTest

class desafio_concrete_solutionsUITests: XCTestCase {
    
    let TIMEOUT : NSTimeInterval = 15
    
    var app : XCUIApplication!
    var tableCells : XCUIElementQuery!
    var genericTests : GenericTests!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        
        self.app = XCUIApplication()
        self.genericTests = GenericTests()
        self.genericTests.initApp()
        self.tableCells = self.app.tables.cells
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    
    //Teste para verificar o clique na celula e exibit detalhes do pull request
    func testClickToPullRequest() {
        self.genericTests.waitLaunchScreenDismiss()
        self.genericTests.waitForElementDisapear(TIMEOUT, handler: nil, evaluatedWithObject: app.buttons["HUD"])
        sleep(1)
        app.tables.cells.elementBoundByIndex(0).tap()
        sleep(1)
        self.genericTests.waitForElementDisapear(TIMEOUT, handler: nil, evaluatedWithObject: app.buttons["HUD"])
        XCTAssertTrue(app.tables.cells.elementBoundByIndex(0).exists)
    }
    
    //Teste para verificar se a paginação esta funcionando corretamente
    func testscrollNextPage() {
        self.genericTests.waitLaunchScreenDismiss()
        self.genericTests.waitForElementDisapear(TIMEOUT, handler: nil, evaluatedWithObject: app.buttons["HUD"])
        sleep(1)
        app.tables.elementBoundByIndex(0).swipeUp()
        let numberOfCellsBefomePaginable = app.tables.elementBoundByIndex(0).cells.count
        app.tables.elementBoundByIndex(0).swipeUp()
        app.tables.elementBoundByIndex(0).swipeUp()
        sleep(5)
        let numberOfCellsAfterPaginable = app.tables.elementBoundByIndex(0).cells.count
        XCTAssertTrue(numberOfCellsAfterPaginable == numberOfCellsBefomePaginable*2)
    }
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
}
